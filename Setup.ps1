# Replaces Windows Terminal's settings.json file with ours

$dir = [System.IO.Path]::GetDirectoryName($myInvocation.MyCommand.Definition)

Copy-Item $dir/settings.json $env:LocalAppData\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\

[System.Environment]::SetEnvironmentVariable('WTREPO', $dir, [System.EnvironmentVariableTarget]::User)