# Windows Terminal settings

Settings and icons for:

* Powershell 7+
* CMD
* Azure Cloud Shell
* Ubuntu
* VS 2019 Dev prompt for Powershell
* C# Interactive
* F# Interactive

## Prerequisites

This assumes you have installed 
[Windows Terminal][wt], and [Powershell 7+][pwsh] (optional).

It also assumes your repos are stored at \Repos, rather than %USERPROFILE%\Source\Repos, due to filename length errors.

## Use

Take a fork of this repository and clone to your local machine.  Then, with your copy, either:

* Run Setup.ps1 (will set WTREPO), or
* Consider the symlink-based approach of [Patrik Svensson][svensson] (you will need to set WTREPO manually).

If Terminal complains about invalid icons, try re-launching it to pick up WTREPO.

If desired, you can mark individual profiles as:

* "hidden": true

## Also

* Be sure to modify file paths in settings.json to match your system, if necessary.
* You may need to modify paths related to the VS Dev Prompt.  You can generally copy from the shortcut you find in the start menu -- just escape the slashes and adjust the quotes, and the instance ID at the end is important.  I include mine, modified to use PWSH.
* Modify the starting folder for Ubuntu to point to your user.

[wt]: https://github.com/Microsoft/Terminal
[pwsh]: https://github.com/Powershell/Powershell
[svensson]: https://www.patriksvensson.se/2019/12/roaming-profiles-with-windows-terminal